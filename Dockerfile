FROM openjdk
RUN echo "hello world"
COPY ./build/libs/docker-task-0.0.1-SNAPSHOT.jar /app/
WORKDIR /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/docker-task-0.0.1.jar"]